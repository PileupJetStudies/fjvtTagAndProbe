#ifndef fjvtTagAndProbe_fjvtTagAndProbe_H
#define fjvtTagAndProbe_fjvtTagAndProbe_H
#include <memory>
#include "JetCalibTools/JetCalibrationTool.h"
#include <EventLoop/Algorithm.h>
#include "GoodRunsLists/GoodRunsListSelectionTool.h"
#include <SampleHandler/ToolsDiscovery.h>
#include "TH1I.h"
#include "TProfile.h"
#include "TH2D.h"
#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "SUSYTools/SUSYCrossSection.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODMissingET/MissingETContainer.h"
#include "JetMomentTools/JetForwardJvtTool.h"
#include "TaggingUtilities/TaggingUtilities.h"

class fjvtTagAndProbe : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;



  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  std::unique_ptr<ST::SUSYObjDef_xAOD> objTool; //!
  CP::TaggingUtilities *tagger; //!

  TTree *jvttree;//!
  float weight;//!
  float xweight;//!
  float prw;//!
  int year;
  int identification;
  int randomRunNumber;
  ULong64_t prwhash;//!
  float muscale;//!
  float NPV;//!
  float MU;//!
  float MUbase;//!
  float VERT;//!
  int pvind0;//!
  int pvind1;//!
  float metx;//!
  float mety;//!
  float mets;//!
  int GRL;//!
  int CLEAN;//!
  int TRIG;//!
  std::vector<float> muonpt;//!
  std::vector<float> muoneta;//!
  std::vector<float> muonphi;//!
  std::vector<float> tmuonpt;//!
  std::vector<float> tmuoneta;//!
  std::vector<float> tmuonphi;//!
  std::vector<float> muonz0;//!
  std::vector<float> muond0;//!
  std::vector<float> muonz0new;//!
  std::vector<float> muonqual;//!
  std::vector<float> muonisol;//!
  std::vector<float> jetpt;//!
  std::vector<float> jetconstpt;//!
  std::vector<float> jetpupt;//!
  std::vector<float> jeteta;//!
  std::vector<float> jetphi;//!
  std::vector<float> jetJVT;//!
  std::vector<float> jetDRPT;//!
  std::vector<float> jetRPT;//!
  std::vector<float> jetJVF;//!
  std::vector<float> jetwidth;//!
  std::vector<float> jetfjvt;//!
  std::vector<float> jetfjvt2;//!
  std::vector<float> jetfjvt975;//!
  std::vector<float> jetfjvt95;//!
  std::vector<float> jetfjvt925;//!
  std::vector<float> jetfjvt90;//!
  std::vector<float> jetfjvt875;//!
  std::vector<float> jetfjvt85;//!
  std::vector<float> jettiming;//!
  std::vector<std::vector<float> > jete_sampl;//!
  std::vector<float> jete_sampl0;//!
  std::vector<int> jetor;//!
  std::vector<int> jettor;//!
  std::vector<int> jetbase;//!
  std::vector<int> jetsel;//!
  std::vector<int> jettype;//!
  std::vector<float> jettruth;//!
  std::vector<int> jetflavor;//!
  std::vector<int> jethsparton;//!
  std::vector<int> jetpuparton;//!
  std::vector<std::vector<float> > clpt;//!
  std::vector<std::vector<float> > cllcpt;//!
  std::vector<std::vector<float> > cleta;//!
  std::vector<std::vector<float> > clphi;//!
  std::vector<std::vector<float> > cltime;//!
  std::vector<std::vector<int> > clinjet;//!
  std::vector<std::vector<float> > clAVG_LAR_Q;//!
  std::vector<std::vector<float> > clAVG_TILE_Q;//!
  std::vector<std::vector<float> > clBADLARQ_FRAC;//!
  std::vector<std::vector<float> > clCENTER_LAMBDA;//!
  std::vector<std::vector<float> > clCENTER_MAG;//!
  std::vector<std::vector<float> > clEM_PROBABILITY;//!
  std::vector<std::vector<float> > clENG_BAD_CELLS;//!
  std::vector<std::vector<float> > clENG_POS;//!
  std::vector<std::vector<float> > clISOLATION;//!
  std::vector<std::vector<float> > clN_BAD_CELLS;//!
  std::vector<std::vector<float> > clSECOND_LAMBDA;//!
  std::vector<std::vector<float> > clSECOND_R;//!
  std::vector<std::vector<std::vector<float> > > cle_sampl;//!
    JetVertexTaggerTool* pjvtag;//!
  std::unique_ptr<SUSY::CrossSectionDB> my_XsecDB; //!

  TH1D *fullweight;//!
  TH1D *passes;//!
  TH1D *ident1;//!
  int IsData;//!
  int IsZero;//!
  GoodRunsListSelectionTool *m_grl; //!
  xAOD::ElectronContainer* electrons_copy; //!
  xAOD::PhotonContainer* photons_copy; //!
  xAOD::MuonContainer* muons_copy; //!
  xAOD::TauJetContainer* taus_copy; //!
  xAOD::JetContainer* jets_copy; //!
  xAOD::MissingETContainer *mettst;//!
  xAOD::MissingETAuxContainer *mettst_aux;//!
  xAOD::ShallowAuxContainer* electrons_copyaux;//!
  xAOD::ShallowAuxContainer* muons_copyaux; //!
  xAOD::ShallowAuxContainer* taus_copyaux; //!
  xAOD::ShallowAuxContainer* photons_copyaux; //!
  xAOD::ShallowAuxContainer* jets_copyaux; //!

  bool handleGRL();
  bool handleLAr();
  bool handleTrig();
  bool handleVertex();
  bool handleHole();
  bool handleBadMuons();
  bool handleJetCleaning();
  bool handleCosmics();
  std::vector<size_t> handleZ();
  std::vector<TLorentzVector> handleZee(std::vector<float> &z0,std::vector<float> &d0);
  bool handleB();
  int getSignalLeptons(std::vector<float> &mlls);
  int getBaselineLeptons(std::vector<float> &mlls);
  EL::StatusCode retrieveAllObjects();

  // this is a standard constructor
  fjvtTagAndProbe ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  JetForwardJvtTool *fjvt;//!

  // this is needed to distribute the algorithm to the workers
  ClassDef(fjvtTagAndProbe, 1);
};

#endif
