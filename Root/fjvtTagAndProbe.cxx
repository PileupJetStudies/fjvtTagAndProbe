#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include "EventLoop/OutputStream.h"
#include <fjvtTagAndProbe/fjvtTagAndProbe.h>
#include "EventPrimitives/EventPrimitivesHelpers.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthPileupEventContainer.h"
#include "xAODCaloEvent/CaloClusterChangeSignalState.h"
#include "xAODBase/IParticleHelpers.h"
#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "PathResolver/PathResolver.h"
#include "TFile.h"
#include "TTree.h"
#include "fastjet/ClusterSequence.hh"
#include "fastjet/ClusterSequenceArea.hh"
#include <fastjet/AreaDefinition.hh>

// this is needed to distribute the algorithm to the workers
ClassImp(fjvtTagAndProbe)

fjvtTagAndProbe :: fjvtTagAndProbe () {}

EL::StatusCode fjvtTagAndProbe :: setupJob (EL::Job& job)
{
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode fjvtTagAndProbe :: histInitialize ()
{
  TEnv rEnv;
  std::string configFile = (PathResolverFindCalibFile("fjvtTagAndProbe/tagProbe.config")).c_str();
  int success = -1;
  success = rEnv.ReadFile(configFile.c_str(),kEnvAll);
  if (success != 0) return EL::StatusCode::FAILURE;
  IsData = rEnv.GetValue("IsData",0);
  IsZero = rEnv.GetValue("IsZero",0);

  passes = new TH1D("passes","passes",30,1,31);
  wk()->addOutput(passes);
  fullweight = new TH1D("fullweight","fullweight",5,1,6);
  wk()->addOutput(fullweight);
  ident1 = new TH1D("ident1","ident1",999999,1,1000000);
  wk()->addOutput(ident1);
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode fjvtTagAndProbe :: fileExecute ()
{
  if (IsData) return EL::StatusCode::SUCCESS;
  xAOD::TEvent* m_event = wk()->xaodEvent();
  TTree *MetaData = dynamic_cast<TTree*>(wk()->inputFile()->Get("MetaData"));
  if (!MetaData) {
    Error("fileExecute()", "MetaData not found! Exiting.");
    return EL::StatusCode::FAILURE;
  }
  MetaData->LoadTree(0);
  bool m_isDerivation = !MetaData->GetBranch("StreamAOD");
  if(m_isDerivation ){
  const xAOD::CutBookkeeperContainer* incompleteCBC = nullptr;
  if(!m_event->retrieveMetaInput(incompleteCBC, "IncompleteCutBookkeepers").isSuccess()){
    Error("initializeEvent()","Failed to retrieve IncompleteCutBookkeepers from MetaData! Exiting.");
    return EL::StatusCode::FAILURE;
  }
  if ( incompleteCBC->size() != 0 ) {
    //Error("initializeEvent()","Found incomplete Bookkeepers! Check file for corruption.");
    //return EL::StatusCode::FAILURE;
  }
  const xAOD::CutBookkeeperContainer* completeCBC = 0;
  if(!m_event->retrieveMetaInput(completeCBC, "CutBookkeepers").isSuccess()){
    Error("initializeEvent()","Failed to retrieve CutBookkeepers from MetaData! Exiting.");
    return EL::StatusCode::FAILURE;
  }
 const xAOD::CutBookkeeper* allEventsCBK = 0;
 int maxCycle = -1;
 for (const auto& cbk: *completeCBC) {
   if (cbk->cycle() > maxCycle && cbk->name() == "AllExecutedEvents" && cbk->inputStream() == "StreamAOD") {
     allEventsCBK = cbk;
     maxCycle = cbk->cycle();
   }
 }

  double sumOfWeights        = allEventsCBK->sumOfEventWeights();
  fullweight->Fill(1.1,sumOfWeights);
  return EL::StatusCode::SUCCESS;
 }
 return EL::StatusCode::SUCCESS;
}

EL::StatusCode fjvtTagAndProbe :: changeInput (bool firstFile)
{
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode fjvtTagAndProbe :: initialize ()
{
  fjvt = new JetForwardJvtTool("fJvt");
  ANA_CHECK(fjvt->initialize());
  my_XsecDB = std::unique_ptr<SUSY::CrossSectionDB>(new SUSY::CrossSectionDB(PathResolverFindCalibFile("SUSYTools/susy_crosssections_13TeV.txt")));

  jvttree = new TTree("jvttree","jvttree");
  jvttree->Branch("weight",&weight,"weight/F");
  jvttree->Branch("xweight",&xweight,"xweight/F");
  jvttree->Branch("prw",&prw,"prw/F");
  jvttree->Branch("year",&year,"year/I");
  jvttree->Branch("identification",&identification,"identification/I");
  jvttree->Branch("randomRunNumber",&randomRunNumber,"randomRunNumber/I");
  jvttree->Branch("prwhash",&prwhash,"prwhash/l");
  jvttree->Branch("muscale",&muscale,"muscale/F");
  jvttree->Branch("NPV",&NPV,"NPV/F");
  jvttree->Branch("MU",&MU,"MU/F");
  jvttree->Branch("MUbase",&MUbase,"MUbase/F");
  jvttree->Branch("VERT",&VERT,"VERT/F");
  jvttree->Branch("pvind0",&pvind0,"pvind0/I");
  jvttree->Branch("pvind1",&pvind1,"pvind1/I");
  jvttree->Branch("GRL",&GRL,"GRL/I");
  jvttree->Branch("CLEAN",&CLEAN,"CLEAN/I");
  jvttree->Branch("TRIG",&TRIG,"TRIG/I");
  jvttree->Branch("muonpt",&muonpt);
  jvttree->Branch("muoneta",&muoneta);
  jvttree->Branch("muonphi",&muonphi);
  //jvttree->Branch("tmuonpt",&tmuonpt);
  //jvttree->Branch("tmuoneta",&tmuoneta);
  //jvttree->Branch("tmuonphi",&tmuonphi);
  jvttree->Branch("muonz0",&muonz0);
  jvttree->Branch("muonz0new",&muonz0new);
  jvttree->Branch("muond0",&muond0);
  jvttree->Branch("muonqual",&muonqual);
  jvttree->Branch("muonisol",&muonisol);
  jvttree->Branch("jetor",&jetor);
  jvttree->Branch("jettor",&jettor);
  jvttree->Branch("jetbase",&jetbase);
  jvttree->Branch("jetsel",&jetsel);
  jvttree->Branch("jetpt",&jetpt);
  jvttree->Branch("jetconstpt",&jetconstpt);
  jvttree->Branch("jetpupt",&jetpupt);
  jvttree->Branch("jeteta",&jeteta);
  jvttree->Branch("jetphi",&jetphi);
  jvttree->Branch("jetJVT",&jetJVT);
  jvttree->Branch("jetDRPT",&jetDRPT);
  jvttree->Branch("jetRPT",&jetRPT);
  jvttree->Branch("jetJVF",&jetJVF);
  jvttree->Branch("jetwidth",&jetwidth);
  jvttree->Branch("jetfjvt",&jetfjvt);
  jvttree->Branch("jetfjvt2",&jetfjvt2);
  jvttree->Branch("jetfjvt975",&jetfjvt975);
  jvttree->Branch("jetfjvt95",&jetfjvt95);
  jvttree->Branch("jetfjvt925",&jetfjvt925);
  jvttree->Branch("jetfjvt90",&jetfjvt90);
  jvttree->Branch("jetfjvt875",&jetfjvt875);
  jvttree->Branch("jetfjvt85",&jetfjvt85);
  jvttree->Branch("jettiming",&jettiming);
  //jvttree->Branch("jete_sampl",&jete_sampl);
  //jvttree->Branch("jete_sampl0",&jete_sampl0);
  jvttree->Branch("metx",&metx,"metx/F");
  jvttree->Branch("mety",&mety,"mety/F");
  jvttree->Branch("mets",&mets,"mets/F");
  if (!IsData) jvttree->Branch("jettype",&jettype);
  if (!IsData) jvttree->Branch("jettruth",&jettruth);
  if (!IsData) jvttree->Branch("jetflavor",&jetflavor);
  if (!IsData) jvttree->Branch("jethsparton",&jethsparton);
  if (!IsData) jvttree->Branch("jetpuparton",&jetpuparton);
  jvttree->Branch("clpt",&clpt);
  jvttree->Branch("cllcpt",&cllcpt);
  jvttree->Branch("cleta",&cleta);
  jvttree->Branch("clphi",&clphi);
  jvttree->Branch("cltime",&cltime);
  jvttree->Branch("clinjet",&clinjet);
  jvttree->Branch("clAVG_LAR_Q",&clAVG_LAR_Q);
  jvttree->Branch("clAVG_TILE_Q",&clAVG_TILE_Q);
  jvttree->Branch("clBADLARQ_FRAC",&clBADLARQ_FRAC);
  jvttree->Branch("clCENTER_LAMBDA",&clCENTER_LAMBDA);
  jvttree->Branch("clCENTER_MAG",&clCENTER_MAG);
  jvttree->Branch("clEM_PROBABILITY",&clEM_PROBABILITY);
  jvttree->Branch("clENG_BAD_CELLS",&clENG_BAD_CELLS);
  jvttree->Branch("clENG_POS",&clENG_POS);
  jvttree->Branch("clISOLATION",&clISOLATION);
  jvttree->Branch("clN_BAD_CELLS",&clN_BAD_CELLS);
  jvttree->Branch("clSECOND_LAMBDA",&clSECOND_LAMBDA);
  jvttree->Branch("clSECOND_R",&clSECOND_R);
  jvttree->Branch("cle_sampl",&cle_sampl);
  wk()->addOutput(jvttree);

  // GRL tool
  m_grl = NULL;
  if(IsData){
    m_grl = new GoodRunsListSelectionTool("GoodRunsListSelectionTool");
    std::vector<std::string> myvals;
    //myvals.push_back(PathResolverFindCalibFile("fjvtTagAndProbe/data15_13TeV.periodAllYear_DetStatus-v79-repro20-02_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.xml"));
    //myvals.push_back(PathResolverFindCalibFile("fjvtTagAndProbe/data16_13TeV.periodAllYear_DetStatus-v83-pro20-14_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml"));
    myvals.push_back(PathResolverFindCalibFile("fjvtTagAndProbe/data17_13TeV.periodAllYear_DetStatus-v96-pro21-12_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"));
    ANA_CHECK( m_grl->setProperty( "GoodRunsListVec", myvals) );
    ANA_CHECK( m_grl->setProperty("PassThrough", false) );
    ANA_CHECK( m_grl->initialize() );
  }

  objTool = std::unique_ptr<ST::SUSYObjDef_xAOD>(new ST::SUSYObjDef_xAOD("SUSYObjDef_xAOD"));
  if (IsData) ANA_CHECK(objTool->setProperty("DataSource",ST::ISUSYObjDef_xAODTool::Data) ) ;
  if (!IsData) ANA_CHECK(objTool->setProperty("DataSource",ST::ISUSYObjDef_xAODTool::FullSim) ) ;

  std::vector<std::string> prw_conf;
  //prw_conf.push_back(PathResolverFindCalibFile("fjvtTagAndProbe/mc16a_defaults_buggy.NotRecommended.prw.root"));
  prw_conf.push_back(PathResolverFindCalibFile("fjvtTagAndProbe/mc16c_defaults.NotRecommended.prw.root"));
  //prw_conf.push_back(PathResolverFindCalibFile("fjvtTagAndProbe/merged_prw_mc16a_latest.root"));
  //prw_conf.push_back(PathResolverFindCalibFile("fjvtTagAndProbe/flatMu.prw.root"));
  ANA_CHECK( objTool->setProperty("PRWConfigFiles", prw_conf) );
  std::vector<std::string> prw_lumicalc;
  //prw_lumicalc.push_back(PathResolverFindCalibFile("fjvtTagAndProbe/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-008.root"));
  prw_lumicalc.push_back(PathResolverFindCalibFile("fjvtTagAndProbe/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-001.root"));
  ANA_CHECK( objTool->setProperty("PRWLumiCalcFiles", prw_lumicalc) );

  ANA_CHECK(objTool->initialize());

  tagger = new CP::TaggingUtilities("tagger");
  ANA_CHECK(tagger->initialize());

  pjvtag = new JetVertexTaggerTool("jvtag");
  ANA_CHECK(pjvtag->setProperty("JVTFileName","JetMomentTools/JVTlikelihood_20140805.root"));
  ANA_CHECK(pjvtag->setProperty("VertexContainer","PrimaryVerticesNew"));
  ANA_CHECK(pjvtag->initialize());

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode fjvtTagAndProbe :: execute ()
{
  muonpt.clear();
  muoneta.clear();
  muonphi.clear();
  tmuonpt.clear();
  tmuoneta.clear();
  tmuonphi.clear();
  muonz0.clear();
  muonz0new.clear();
  muond0.clear();
  muonqual.clear();
  muonisol.clear();
  jetor.clear();
  jettor.clear();
  jetbase.clear();
  jetsel.clear();
  jetpt.clear();
  jetconstpt.clear();
  jetpupt.clear();
  jeteta.clear();
  jetphi.clear();
  jetJVT.clear();
  jetDRPT.clear();
  jetRPT.clear();
  jetJVF.clear();
  jetwidth.clear();
  jetfjvt.clear();
  jetfjvt2.clear();
  jetfjvt975.clear();
  jetfjvt95.clear();
  jetfjvt925.clear();
  jetfjvt90.clear();
  jetfjvt875.clear();
  jetfjvt85.clear();
  jettiming.clear();
  jete_sampl.clear();
  jete_sampl0.clear();
  jettype.clear();
  jettruth.clear();
  jetflavor.clear();
  jethsparton.clear();
  jetpuparton.clear();
  clpt.clear();
  cllcpt.clear();
  cleta.clear();
  clphi.clear();
  cltime.clear();
  clinjet.clear();
  clAVG_LAR_Q.clear();
  clAVG_TILE_Q.clear();
  clBADLARQ_FRAC.clear();
  clCENTER_LAMBDA.clear();
  clCENTER_MAG.clear();
  clEM_PROBABILITY.clear();
  clENG_BAD_CELLS.clear();
  clENG_POS.clear();
  clISOLATION.clear();
  clN_BAD_CELLS.clear();
  clSECOND_LAMBDA.clear();
  clSECOND_R.clear();
  cle_sampl.clear();

  xAOD::TEvent* store = wk()->xaodEvent();
  const xAOD::EventInfo* ei = 0;
  ANA_CHECK(store->retrieve( ei, "EventInfo" ) );
  weight = (!IsData && ei->mcEventWeights().size() > 0) ? (ei->mcEventWeights())[0] : 1.;
  xweight = IsData?1:my_XsecDB->xsectTimesEff(ei->mcChannelNumber());

  identification = IsData?1:ei->mcChannelNumber();
  ident1->SetBinContent(identification,fullweight->GetBinContent(1));
  ANA_CHECK(objTool->ApplyPRWTool());
  MU = objTool->GetCorrectedAverageInteractionsPerCrossing();
  MUbase = ei->averageInteractionsPerCrossing();
  prw = IsData?1:objTool->GetPileupWeight();
  prwhash = IsData?1:objTool->GetPileupWeightHash();
  year = (IsData?0:objTool->treatAsYear());
  int runNumber = ei->runNumber();
  randomRunNumber = (IsData?runNumber:objTool->GetRandomRunNumber());
  fullweight->Fill(2.1,prw);
  fullweight->Fill(3.1,weight);
  fullweight->Fill(4.1,prw*weight);
  fullweight->Fill(5.1);

  const xAOD::VertexContainer* pvertices = 0;
  NPV = 0;
  ANA_CHECK( store->retrieve( pvertices, "PrimaryVertices" ) );
  for(const auto& vx : *pvertices) {
    if ((vx->vertexType() == 1) || (vx->vertexType() == 3)) ++NPV;
  }

  //GRL calculation
  bool n_grl = handleGRL();

  GRL = n_grl;

  //LAr error
  bool n_lar = handleLAr();

  //Good vertex
  bool n_vertex = handleVertex();
  if (!n_vertex) return EL::StatusCode::SUCCESS;

  //Now, the rest of the criteria require some object processing
  if (retrieveAllObjects()==EL::StatusCode::FAILURE) return EL::StatusCode::FAILURE;
  metx = (*mettst)["Final"]->mpx();
  mety = (*mettst)["Final"]->mpy();
  mets = (*mettst)["Final"]->sumet();
  delete mettst;
  delete mettst_aux;

  //Perform overlap removal
  if (n_vertex) ANA_CHECK( objTool->OverlapRemoval(electrons_copy, muons_copy, jets_copy,photons_copy) );

  //Trigger
  bool n_trig = handleTrig();
  TRIG = n_trig;

  //Jet in hole TODO
  bool n_hole = handleHole();

  //Bad muons
  bool n_badMu = handleBadMuons();

  //Jet cleaning
  bool n_clean = handleJetCleaning();

  //Handle cosmics TODO
  bool n_cosmic = handleCosmics();

  CLEAN = (n_badMu && n_clean && n_cosmic);

  bool mark = false;
  VERT = 0;
  const xAOD::TruthEventContainer *tec = 0;
  if (!IsData && objTool->GetPrimVtx()) {
    ANA_CHECK(store->retrieve(tec,"TruthEvents"));
    if (fabs(objTool->GetPrimVtx()->z()-(*tec->begin())->signalProcessVertex()->z())<0.1) mark = true;
    if (mark) VERT = 1; 
  }
  if (!IsData) for(const auto& tpe : *tec) {
    for (size_t i = 0; i < tpe->nTruthParticles(); i++) {
      const xAOD::TruthParticle *part = static_cast<const xAOD::TruthParticle*>(tpe->truthParticle(i));
      if (!part) continue;
      if (abs(part->pdgId())!=13 || part->status()!=1) continue;
      if (part->pt()<10e3) continue;
      if (!part->parent() || part->parent()->pdgId()!=23) continue;
      tmuonpt.push_back(part->pt());
      tmuoneta.push_back(part->eta());
      tmuonphi.push_back(part->phi());
    }
  }

  std::vector<size_t> zparts = handleZ();
  bool n_hasz = (zparts.size()==2);

  muscale = objTool->GetTotalMuonSF(*muons_copy,true,true,"");
  passes->Fill(1,weight*muscale);
  //if (n_grl)      passes->Fill(2,weight*muscale);else return EL::StatusCode::SUCCESS;
  //if (n_lar)      passes->Fill(3,weight*muscale);else return EL::StatusCode::SUCCESS;
  //if (n_hole)     passes->Fill(4,weight*muscale);else return EL::StatusCode::SUCCESS;
  //if (n_trig)     passes->Fill(5,weight*muscale);else return EL::StatusCode::SUCCESS;
  if (n_vertex)   passes->Fill(6,weight*muscale);else return EL::StatusCode::SUCCESS;
  if (n_clean)    passes->Fill(7,weight*muscale);else return EL::StatusCode::SUCCESS;
  if (n_cosmic)   passes->Fill(8,weight*muscale);else return EL::StatusCode::SUCCESS;
  if (n_badMu)    passes->Fill(9,weight*muscale);else return EL::StatusCode::SUCCESS;
  if (n_hasz || IsZero)     passes->Fill(10,weight*muscale);else return EL::StatusCode::SUCCESS;

  const xAOD::MissingETContainer* trkMet  = nullptr;
  ANA_CHECK(store->retrieve(trkMet, "MET_Track") );

  const xAOD::CaloClusterContainer* clusters  = nullptr;
  //ANA_CHECK(store->retrieve(clusters, "CaloCalTopoClusters") );


  if (!IsData && !IsZero) tagger->truthTag(jets_copy,false);

  int closestVert0 = -1;
  int closestVert1 = -1;
  for (const auto& muon : *muons_copy) if (muon->auxdata<char>("baseline")==1) {
    muonpt.push_back(muon->pt()/1e3);
    muoneta.push_back(muon->eta());
    muonphi.push_back(muon->phi());
    muonz0.push_back(muon->auxdata<float>("z0sinTheta"));
    muond0.push_back(muon->auxdata<float>("d0sig"));
    muonqual.push_back(muon->auxdata<char>("signal"));
    muonisol.push_back(muon->auxdata<char>("isol"));
    float closestz0 = 1e9;
    for (const auto& vx : *pvertices) {
      float current = fabs((muon->primaryTrackParticle()->z0()-vx->z())*sin(muon->primaryTrackParticle()->theta()));
      if(vx->vertexType()!=xAOD::VxType::PriVtx && vx->vertexType()!=xAOD::VxType::PileUp) continue;
      if (current>closestz0) continue;
      closestz0 = current;
      if (!IsZero && muon->index()==zparts[0]) closestVert0 = vx->index();
      if (!IsZero && muon->index()==zparts[1]) closestVert1 = vx->index();
    }
  }
  tagger->chooseNewPV(closestVert0);
  pjvtag->modify(*jets_copy);
  fjvt->modify(*jets_copy);
  //if (closestVert0==closestVert1 && closestVert0>0) tagger->chooseNewPV(closestVert0);
  pvind0 = closestVert0;
  pvind1 = closestVert1;

  for (const auto& muon : *muons_copy) if (muon->auxdata<char>("baseline")==1) {
    muonz0new.push_back(pvind0<=0?muon->auxdata<float>("z0sinTheta"):fabs((muon->primaryTrackParticle()->z0()-pvertices->at(pvind0)->z())*muon->primaryTrackParticle()->theta()));
  }


  const xAOD::VertexContainer* pvertices2 = 0;
  ANA_CHECK( store->retrieve( pvertices2, "PrimaryVerticesNew" ) );
  tagger->buildEMTopoJets(IsZero,"PrimaryVerticesNew");

  std::vector<TVector2> vertexMet,vertexMet975,vertexMet95,vertexMet925,vertexMet90,vertexMet875,vertexMet85;

  for(const auto& vx : *pvertices) {
    if(vx->vertexType()!=xAOD::VxType::PriVtx && vx->vertexType()!=xAOD::VxType::PileUp) continue;
    TString vname = "PVTrack_vx";
    vname += vx->index();
    TString jname = "AntiKt4EMTopoJets";
    jname += vx->index();
    const xAOD::JetContainer* vertex_jets  = nullptr;
    ANA_CHECK(store->retrieve(vertex_jets,jname.Data()) );
    vertexMet.push_back(-(vx->index()==0?0:1)/0.4*TVector2(0.5*(*trkMet)[vname.Data()]->mpx(),0.5*(*trkMet)[vname.Data()]->mpy()));
    vertexMet975.push_back(-(vx->index()==0?0:1)/0.4*TVector2(0.5*(*trkMet)[vname.Data()]->mpx(),0.5*(*trkMet)[vname.Data()]->mpy()));
    vertexMet95.push_back(-(vx->index()==0?0:1)/0.4*TVector2(0.5*(*trkMet)[vname.Data()]->mpx(),0.5*(*trkMet)[vname.Data()]->mpy()));
    vertexMet925.push_back(-(vx->index()==0?0:1)/0.4*TVector2(0.5*(*trkMet)[vname.Data()]->mpx(),0.5*(*trkMet)[vname.Data()]->mpy()));
    vertexMet90.push_back(-(vx->index()==0?0:1)/0.4*TVector2(0.5*(*trkMet)[vname.Data()]->mpx(),0.5*(*trkMet)[vname.Data()]->mpy()));
    vertexMet875.push_back(-(vx->index()==0?0:1)/0.4*TVector2(0.5*(*trkMet)[vname.Data()]->mpx(),0.5*(*trkMet)[vname.Data()]->mpy()));
    vertexMet85.push_back(-(vx->index()==0?0:1)/0.4*TVector2(0.5*(*trkMet)[vname.Data()]->mpx(),0.5*(*trkMet)[vname.Data()]->mpy()));
    for (const auto& jet : *vertex_jets) {
      vertexMet[vertexMet.size()-1] += TVector2(0.5*jet->pt()*cos(jet->phi()),0.5*jet->pt()*sin(jet->phi()));
      if (gRandom->Rndm()<0.975) vertexMet975[vertexMet975.size()-1] += TVector2(0.5*jet->pt()*cos(jet->phi()),0.5*jet->pt()*sin(jet->phi()));
      if (gRandom->Rndm()<0.95) vertexMet95[vertexMet95.size()-1] += TVector2(0.5*jet->pt()*cos(jet->phi()),0.5*jet->pt()*sin(jet->phi()));
      if (gRandom->Rndm()<0.925) vertexMet925[vertexMet925.size()-1] += TVector2(0.5*jet->pt()*cos(jet->phi()),0.5*jet->pt()*sin(jet->phi()));
      if (gRandom->Rndm()<0.90) vertexMet90[vertexMet90.size()-1] += TVector2(0.5*jet->pt()*cos(jet->phi()),0.5*jet->pt()*sin(jet->phi()));
      if (gRandom->Rndm()<0.875) vertexMet875[vertexMet875.size()-1] += TVector2(0.5*jet->pt()*cos(jet->phi()),0.5*jet->pt()*sin(jet->phi()));
      if (gRandom->Rndm()<0.85) vertexMet85[vertexMet85.size()-1] += TVector2(0.5*jet->pt()*cos(jet->phi()),0.5*jet->pt()*sin(jet->phi()));
    }
  }

  for (const auto& jet : *jets_copy) {
    //if (fabs(jet->eta())<2.4) continue;
    if (fabs(jet->pt())<20e3) continue;
    int hasmu = 0;
    if (!IsData) for(const auto& tpe : *tec) {
      for (size_t i = 0; i < tpe->nTruthParticles(); i++) {
        const xAOD::TruthParticle *part = static_cast<const xAOD::TruthParticle*>(tpe->truthParticle(i));
        if (!part) continue;
        if (abs(part->pdgId())!=13 || part->status()!=1) continue;
        if (!part->parent() || part->parent()->pdgId()!=23) continue;
        if (part->p4().DeltaR(jet->p4())<0.4) hasmu = true;
      }
    }
    jetor.push_back(jet->auxdata<char>("passOR"));
    jettor.push_back(hasmu);
    jetbase.push_back(jet->auxdata<char>("baseline"));
    jetsel.push_back(jet->auxdata<char>("selected"));
    jetpt.push_back(jet->pt()/1e3);
    float jpt = 0;
    float cpt = 0;
    jet->getAttribute<float>("JetPileupScaleMomentum_pt",jpt);
    jet->getAttribute<float>("JetConstitScaleMomentum_pt",cpt);
    jetconstpt.push_back(cpt/1e3);
    jetpupt.push_back(jpt/1e3);
    jeteta.push_back(jet->eta());
    jetphi.push_back(jet->phi());
    float jvt = 0;
    jet->getAttribute<float>("Jvt",jvt);
    jetJVT.push_back(jvt);
    float jvf = 0;
    jet->getAttribute<float>("JvtJvfcorr",jvf);
    jetJVF.push_back(jvf);
    float rpt = 0;
    jet->getAttribute<float>("JvtRpt",rpt);
    jetRPT.push_back(rpt*jpt/jet->pt());
    float Width = 0;
    jet->getAttribute(xAOD::JetAttribute::Width,Width);
    jetwidth.push_back(Width);
    jetDRPT.push_back(fjvt->getDrpt(jet));
    jetfjvt.push_back(jet->auxdata<float>("fJvt"));

    TVector2 fjet(-jet->pt()*cos(jet->phi()),-jet->pt()*sin(jet->phi()));
    double new_fjvt = 0;
    int new_ind = -1;
    double new_fjvt975 = 0;
    double new_fjvt95 = 0;
    double new_fjvt925 = 0;
    double new_fjvt90 = 0;
    double new_fjvt875 = 0;
    double new_fjvt85 = 0;
    for (size_t pui = 0; pui < vertexMet.size(); pui++) {
      if (!IsZero && pvertices2->at(pui)->vertexType()==xAOD::VxType::PriVtx) continue;
      double projection = vertexMet[pui]*fjet/fjet.Mod();
      if (projection>new_fjvt) {new_ind = pui; new_fjvt = projection;}
      double projection975 = vertexMet975[pui]*fjet/fjet.Mod();
      if (projection975>new_fjvt975) new_fjvt975 = projection975;
      double projection95 = vertexMet95[pui]*fjet/fjet.Mod();
      if (projection95>new_fjvt95) new_fjvt95 = projection95;
      double projection925 = vertexMet925[pui]*fjet/fjet.Mod();
      if (projection925>new_fjvt925) new_fjvt925 = projection925;
      double projection90 = vertexMet90[pui]*fjet/fjet.Mod();
      if (projection90>new_fjvt90) new_fjvt90 = projection90;
      double projection875 = vertexMet875[pui]*fjet/fjet.Mod();
      if (projection875>new_fjvt875) new_fjvt875 = projection875;
      double projection85 = vertexMet85[pui]*fjet/fjet.Mod();
      if (projection85>new_fjvt85) new_fjvt85 = projection85;
    }

    jetfjvt2.push_back(new_fjvt/jet->pt());
    jetfjvt975.push_back(new_fjvt975/jet->pt());
    jetfjvt95.push_back(new_fjvt95/jet->pt());
    jetfjvt925.push_back(new_fjvt925/jet->pt());
    jetfjvt90.push_back(new_fjvt90/jet->pt());
    jetfjvt875.push_back(new_fjvt875/jet->pt());
    jetfjvt85.push_back(new_fjvt85/jet->pt());
    jettiming.push_back(jet->auxdata<float>("Timing"));
    std::vector<float> energies = jet->auxdata<std::vector<float> >("EnergyPerSampling");
    jete_sampl.push_back(energies);
    float allenergy = 0;
    for (size_t i = 0; i < energies.size(); i++) allenergy+=energies[i];
    jete_sampl0.push_back(energies[21]/allenergy);
    int tagvalue = 0;
    if (jet->auxdata<char>("hs")) tagvalue = 1;
    else if (jet->auxdata<char>("qcd")) tagvalue = 3;
    else if (jet->auxdata<char>("stochastic")) tagvalue = 4;
    else if (jet->auxdata<char>("pu")) tagvalue = 5;
    else  tagvalue = 2;
    jettype.push_back(tagvalue);
    jettruth.push_back(tagvalue==1?jet->auxdata<float>("hsPt"):std::max<float>(jet->auxdata<float>("hsPt"),jet->auxdata<float>("qcdPt")));
    int flavor = 0;
    if (!IsData) jet->getAttribute<int>("PartonTruthLabelID",flavor);
    jetflavor.push_back(flavor);
    jethsparton.push_back(jet->auxdata<int>("leadPartonHS"));
    jetpuparton.push_back(jet->auxdata<int>("leadPartonPU"));

    clpt.push_back(std::vector<float>());
    cllcpt.push_back(std::vector<float>());
    cleta.push_back(std::vector<float>());
    clphi.push_back(std::vector<float>());
    cltime.push_back(std::vector<float>());
    clinjet.push_back(std::vector<int>());
    clAVG_LAR_Q.push_back(std::vector<float>());
    clAVG_TILE_Q.push_back(std::vector<float>());
    clBADLARQ_FRAC.push_back(std::vector<float>());
    clCENTER_LAMBDA.push_back(std::vector<float>());
    clCENTER_MAG.push_back(std::vector<float>());
    clEM_PROBABILITY.push_back(std::vector<float>());
    clENG_BAD_CELLS.push_back(std::vector<float>());
    clENG_POS.push_back(std::vector<float>());
    clISOLATION.push_back(std::vector<float>());
    clN_BAD_CELLS.push_back(std::vector<float>());
    clSECOND_LAMBDA.push_back(std::vector<float>());
    clSECOND_R.push_back(std::vector<float>());
    cle_sampl.push_back(std::vector<std::vector<float> >());

    float EMFrac = 0;
    jet->getAttribute(xAOD::JetAttribute::EMFrac,EMFrac);

    float part0 = 0;
    float part1 = 0;
    float part2 = 0;
    if (false) for (size_t i = 0; i < jet->numConstituents(); i++) {
      const xAOD::CaloCluster *cl = clusters->at(jet->constituentLinks()[i].index());
      if (!cl) continue;
      //part0 += cl->auxdata<std::vector<float> >("e_sampl")[0];
      //part1 += cl->auxdata<std::vector<float> >("e_sampl")[1];
      //part2 += cl->auxdata<std::vector<float> >("e_sampl")[2];
      clpt[clpt.size()-1].push_back(cl->rawE()/cosh(cl->rawEta()));
      cllcpt[cllcpt.size()-1].push_back(cl->calE()/cosh(cl->calEta()));
      cleta[cleta.size()-1].push_back(cl->eta());
      clphi[clphi.size()-1].push_back(cl->phi());
      cltime[cltime.size()-1].push_back(cl->time());
      clinjet[clinjet.size()-1].push_back(1);
      clAVG_LAR_Q[clAVG_LAR_Q.size()-1].push_back(cl->auxdata<float>("AVG_LAR_Q"));
      clAVG_TILE_Q[clAVG_TILE_Q.size()-1].push_back(cl->auxdata<float>("AVG_TILE_Q"));
      clBADLARQ_FRAC[clBADLARQ_FRAC.size()-1].push_back(cl->auxdata<float>("BADLARQ_FRAC"));
      clCENTER_LAMBDA[clCENTER_LAMBDA.size()-1].push_back(cl->auxdata<float>("CENTER_LAMBDA"));
      clCENTER_MAG[clCENTER_MAG.size()-1].push_back(cl->auxdata<float>("CENTER_MAG"));
      clEM_PROBABILITY[clEM_PROBABILITY.size()-1].push_back(cl->auxdata<float>("EM_PROBABILITY"));
      clENG_BAD_CELLS[clENG_BAD_CELLS.size()-1].push_back(cl->auxdata<float>("ENG_BAD_CELLS"));
      clENG_POS[clENG_POS.size()-1].push_back(cl->auxdata<float>("ENG_POS"));
      clISOLATION[clISOLATION.size()-1].push_back(cl->auxdata<float>("ISOLATION"));
      clN_BAD_CELLS[clN_BAD_CELLS.size()-1].push_back(cl->auxdata<float>("N_BAD_CELLS"));
      clSECOND_LAMBDA[clSECOND_LAMBDA.size()-1].push_back(cl->auxdata<float>("SECOND_LAMBDA"));
      clSECOND_R[clSECOND_R.size()-1].push_back(cl->auxdata<float>("SECOND_R"));
      cle_sampl[cle_sampl.size()-1].push_back(cl->auxdata<std::vector<float> >("e_sampl"));
    }
    if (false) for (const auto& cl : *clusters) {
      if (cl->p4().DeltaR(jet->p4())>0.6) continue;
      bool breaker = false;
      for (size_t i = 0; i < jet->numConstituents(); i++) {
        const xAOD::CaloCluster *cl2 = clusters->at(jet->constituentLinks()[i].index());
        if (cl==cl2) breaker = true;
      }
      if (breaker) continue;
      clpt[clpt.size()-1].push_back(cl->rawE()/cosh(cl->rawEta()));
      cllcpt[cllcpt.size()-1].push_back(cl->calE()/cosh(cl->calEta()));
      cleta[cleta.size()-1].push_back(cl->eta());
      clphi[clphi.size()-1].push_back(cl->phi());
      cltime[cltime.size()-1].push_back(cl->time());
      clinjet[clinjet.size()-1].push_back(0);
      clAVG_LAR_Q[clAVG_LAR_Q.size()-1].push_back(cl->auxdata<float>("AVG_LAR_Q"));
      clAVG_TILE_Q[clAVG_TILE_Q.size()-1].push_back(cl->auxdata<float>("AVG_TILE_Q"));
      clBADLARQ_FRAC[clBADLARQ_FRAC.size()-1].push_back(cl->auxdata<float>("BADLARQ_FRAC"));
      clCENTER_LAMBDA[clCENTER_LAMBDA.size()-1].push_back(cl->auxdata<float>("CENTER_LAMBDA"));
      clCENTER_MAG[clCENTER_MAG.size()-1].push_back(cl->auxdata<float>("CENTER_MAG"));
      clEM_PROBABILITY[clEM_PROBABILITY.size()-1].push_back(cl->auxdata<float>("EM_PROBABILITY"));
      clENG_BAD_CELLS[clENG_BAD_CELLS.size()-1].push_back(cl->auxdata<float>("ENG_BAD_CELLS"));
      clENG_POS[clENG_POS.size()-1].push_back(cl->auxdata<float>("ENG_POS"));
      clISOLATION[clISOLATION.size()-1].push_back(cl->auxdata<float>("ISOLATION"));
      clN_BAD_CELLS[clN_BAD_CELLS.size()-1].push_back(cl->auxdata<float>("N_BAD_CELLS"));
      clSECOND_LAMBDA[clSECOND_LAMBDA.size()-1].push_back(cl->auxdata<float>("SECOND_LAMBDA"));
      clSECOND_R[clSECOND_R.size()-1].push_back(cl->auxdata<float>("SECOND_R"));
      cle_sampl[cle_sampl.size()-1].push_back(cl->auxdata<std::vector<float> >("e_sampl"));

    }
  }

  jvttree->Fill();
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode fjvtTagAndProbe :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode fjvtTagAndProbe :: finalize ()
{
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode fjvtTagAndProbe :: histFinalize ()
{
  return EL::StatusCode::SUCCESS;
}

bool fjvtTagAndProbe::handleGRL() {
  xAOD::TEvent* store = wk()->xaodEvent();
  const xAOD::EventInfo* ei = 0;
  ANA_CHECK( store->retrieve( ei, "EventInfo" ) );
  return !m_grl || m_grl->passRunLB(ei->runNumber(), ei->lumiBlock());
}

bool fjvtTagAndProbe::handleLAr() {
  xAOD::TEvent* store = wk()->xaodEvent();
  const xAOD::EventInfo* ei = 0;
  ANA_CHECK( store->retrieve( ei, "EventInfo" ) );
  return !m_grl || ei->errorState(xAOD::EventInfo::LAr)!=xAOD::EventInfo::Error;
}

bool fjvtTagAndProbe::handleHole() {
  //TODO
  return true;
}

bool fjvtTagAndProbe::handleTrig() {
  std::string trigItem[2]={"HLT_mu24_iloose_L1MU15","HLT_mu50"};
  bool passed = false;
  bool passedPrime = false;
  for(int it=0; it<2; it++){
    float prescale = objTool->GetTrigPrescale(trigItem[it]);
    passed = passed || objTool->IsTrigPassed(trigItem[it]);
  }
  return passed;
}

bool fjvtTagAndProbe::handleVertex() {
  //bool hasNoVertex = !IsZero && !objTool->GetPrimVtx();
  bool hasNoVertex = !objTool->GetPrimVtx();
  return !hasNoVertex;
}

bool fjvtTagAndProbe::handleBadMuons() {
  for (const auto& muon : *muons_copy) if (muon->auxdata<char>("baseline")==1 && objTool->IsBadMuon(*muon,0.2)) return false;
  //for (const auto& muon : *muons_copy) if (objTool->IsSignalMuon(*muon,10e3) && muon->auxdata<char>("baseline")==1 && objTool->IsBadMuon(*muon)) return false;
  return true;
}

bool fjvtTagAndProbe::handleJetCleaning() {
  for (const auto& jet : *jets_copy) {
    std::vector<float> VertexFrac;
    jet->getAttribute(xAOD::JetAttribute::JVF , VertexFrac);
    //if (jet->pt()<50000 && fabs(jet->eta())<2.4 && VertexFrac[0]<0.25) continue;
    if (objTool->IsBadJet(*jet)) return false; 
  }
  return true;
}

bool fjvtTagAndProbe::handleB() {
  for (const auto& jet : *jets_copy) {
    if (jet->auxdata<char>("passOR")==1 && jet->auxdata<char>("baseline")==1 && objTool->IsBJet(*jet)) return false; 
  }
  return true;
}

bool fjvtTagAndProbe::handleCosmics() {
  //return true; //TODO
  for (const auto& muon : *muons_copy) if (muon->auxdata<char>("baseline")==1 && objTool->IsCosmicMuon(*muon,1,0.2)) return false;
  return true;
}

std::vector<size_t> fjvtTagAndProbe::handleZ() {
  static SG::AuxElement::Decorator<float> dec_z0sinTheta("z0sinTheta");
  static SG::AuxElement::Decorator<float> dec_d0sig("d0sig");
  std::vector<size_t> allpart;
  double bestmass = 25e3;
  //for (const auto& muon : *muons_copy) if (muon->auxdata<char>("passOR") && objTool->IsSignalMuon(*muon,25e3,3.,0.5)) {
  //  for (const auto& muon2 : *muons_copy) if (muon2->auxdata<char>("passOR") && objTool->IsSignalMuon(*muon2,25e3,3.,0.5)) {
  for (const auto& muon : *muons_copy) if (muon->auxdata<char>("baseline")) {
    for (const auto& muon2 : *muons_copy) if (muon2->auxdata<char>("baseline")) {
      if (muon->charge()*muon2->charge()>=0 || muon2->pt()>muon->pt()) continue;
      double currmass = (muon->p4()+muon2->p4()).M();
      if (fabs(currmass-91187.6)>bestmass) continue;
      allpart.clear();
      bestmass = fabs(currmass-91187.6);
      allpart.push_back(muon->index());
      allpart.push_back(muon2->index());
    }
  }
  return allpart; 
}

std::vector<TLorentzVector> fjvtTagAndProbe::handleZee(std::vector<float> &z0,std::vector<float> &d0) {
  static SG::AuxElement::Decorator<float> dec_z0sinTheta("z0sinTheta");
  static SG::AuxElement::Decorator<float> dec_d0sig("d0sig");
  std::vector<TLorentzVector> allpart;
  double bestmass = 1000e3;
  for (const auto& electron : *electrons_copy) if (electron->auxdata<char>("passOR") && objTool->IsSignalElectron(*electron,25e3,5.,0.5)) {
    for (const auto& electron2 : *electrons_copy) if (electron2->auxdata<char>("passOR") && objTool->IsSignalElectron(*electron2,25e3,5.,0.5)) {
      if (electron->charge()*electron2->charge()>=0 || electron2->pt()>electron->pt()) continue;
      double currmass = (electron->p4()+electron2->p4()).M();
      if (fabs(currmass-91187.6)>bestmass) continue;
      allpart.clear();
      bestmass = fabs(currmass-91187.6);
      allpart.push_back(electron->p4());
      allpart.push_back(electron2->p4());
      z0.push_back(fabs(electron->auxdata<float>("z0sinTheta")));
      z0.push_back(fabs(electron2->auxdata<float>("z0sinTheta")));
      d0.push_back(fabs(electron->auxdata<float>("d0sig")));
      d0.push_back(fabs(electron2->auxdata<float>("d0sig")));
    }
  }
  return allpart; 
}

EL::StatusCode fjvtTagAndProbe::retrieveAllObjects() {
  // Get the Electrons from the event
  electrons_copy = NULL;
  electrons_copyaux = 0;
  ANA_CHECK( objTool->GetElectrons(electrons_copy,electrons_copyaux,true) );

  // Get the Muons from the event
  muons_copy = NULL;
  muons_copyaux = 0;
  ANA_CHECK( objTool->GetMuons(muons_copy,muons_copyaux,true) );

  // Get the Photons from the event
  photons_copy = NULL;
  photons_copyaux = 0;
  ANA_CHECK( objTool->GetPhotons(photons_copy,photons_copyaux) );

  // Get the Jets from the event
  jets_copy = NULL;
  jets_copyaux = 0;
  ANA_CHECK( objTool->GetJets(jets_copy,jets_copyaux,true,"AntiKt4EMTopoJets") );

  // Calculate MET
  mettst = new xAOD::MissingETContainer;
  mettst_aux = new xAOD::MissingETAuxContainer;
  mettst->setStore(mettst_aux);
  ANA_CHECK( objTool->GetMET(*mettst,jets_copy,electrons_copy,muons_copy,photons_copy) );
  return EL::StatusCode::SUCCESS;
}
